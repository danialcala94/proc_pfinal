// Author: ALCAL� VALERA, DANIEL

package es.unileon.procesadores.parsers;

import java_cup.runtime.*;
import java.io.*;
import es.unileon.procesadores.symbols.*;
%%
%{
 private SymbolsTable tabla;
 public Yylex(Reader in, SymbolsTable t){
 this(in);
 this.tabla = t;
 }
 public SymbolsTable getTabla(){
 	return tabla;
 }
 public int linea(){return yyline+1;}
 public int columna(){ return yycolumn+1;}
 
%}
%unicode
%cup
%line
%column
%%

"@endsection" { return new Symbol(sym.ENDSECTION); }
"void main()" { return new Symbol(sym.MAINFUNCTION); }
/* Ignoring Comments */
[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/] { System.out.println("--- Multiline comment detected. Ignoring it. ---"); }
"//".* { System.out.println("--- Singleline comment detected. Ignoring it. ---"); }

[:digit:]+ { return new Symbol(sym.VALOR, new MySymbol(yytext(), null)); }

/* Exceptions */
"\z" { System.out.println("Salto l�nea"); }
"\n" { System.out.println("Salto l�nea"); }
"\b" { System.out.println("Salto l�nea"); }
"++" { return new Symbol(sym.INCREMENTADOR); }
"+" { return new Symbol(sym.MAS); }
"--" { return new Symbol(sym.DECREMENTADOR); }
"-" { return new Symbol(sym.MENOS); }
"%" { return new Symbol(sym.MODULO); }
"**" { return new Symbol(sym.POTENCIA); }
"*" { return new Symbol(sym.POR); }
"/" { return new Symbol(sym.ENTRE); }
";" { return new Symbol(sym.PUNTOYCOMA); }
"(" { return new Symbol(sym.ABREPAR); }
")" { return new Symbol(sym.CIERRAPAR); }
"==" { return new Symbol(sym.IGUAL); }
"=" { return new Symbol(sym.ASIGNA); }
"{" { return new Symbol(sym.ABRELLAVE); }
"}" {  return new Symbol(sym.CIERRALLAVE); }
"[" { return new Symbol(sym.ABRECORCHETE); }
"]" {  return new Symbol(sym.CIERRACORCHETE); }
"<=" { return new Symbol(sym.MENOROIGUAL); }
"<" { return new Symbol(sym.MENOR); }
">=" { return new Symbol(sym.MAYOROIGUAL); }
">" { return new Symbol(sym.MAYOR); }
"!=" { return new Symbol(sym.DISTINTO); }
"," { return new Symbol(sym.COMA); }
"\"" { return new Symbol(sym.COMILLASDOBLES); }
"&&" { return new Symbol(sym.AND); }
"||" { return new Symbol(sym.OR); }

"return" { return new Symbol(sym.RETURN); }
"printf" { return new Symbol(sym.PRINTF); }
"scanf" { return new Symbol(sym.SCANF); }

/* SENTENCIAS DE CONTROL */
"if" { return new Symbol(sym.IF); }
"else" { return new Symbol(sym.ELSE);}
"while" { return new Symbol(sym.WHILE); }
"do" { return new Symbol(sym.DO); }
"for" { return new Symbol(sym.FOR); }


/* TIPOS */
"int" { return new Symbol(sym.INT); }
"bool" { return new Symbol(sym.BOOLEAN); }
"float" { return new Symbol(sym.FLOAT); }
"char" { return new Symbol(sym.CHAR); }

/* OTROS */
"#include" { return new Symbol(sym.INCLUDE); }
[[:jletter:][.]]+[.][h] {
	MySymbol s;
	s = new MySymbol(yytext().substring(0, yytext().length() - 2), null);
	return new Symbol(sym.VARLIBRERIA, s);
}
"\""[[:jletterdigit:][% ]]*"\"" { return new Symbol(sym.TEXTO, new MySymbol(yytext(), null)); }
"&"[:jletter:][:jletterdigit:]* {
	MySymbol s;
	// We take off the ampersand
	s = new MySymbol(yytext().substring(1, yytext().length()), null);
	return new Symbol(sym.VARIABLE_MEM, s);
}
[:jletter:][:jletterdigit:]* {
	MySymbol s;
	s = new MySymbol(yytext(), null);
	return new Symbol(sym.VARIABLE, s);
}


[ \t\r\n]+ {;}

/*<<EOF>> { System.out.println("   >>>   EOF   <<<   "); yyterminate(); }*/

. { System.err.println("  �� Error en l�xico -> "+yytext()+" !!"); }
