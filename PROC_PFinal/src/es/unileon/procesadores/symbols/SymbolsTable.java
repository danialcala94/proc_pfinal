package es.unileon.procesadores.symbols;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

import es.unileon.procesadores.objects.VariableOrFunction;

public class SymbolsTable {

	// Apuntar� a la tabla de s�mbolos local
	private HashMap t;
	// Cada vez que encontremos un nuevo bloque que anidar, lo tendremos aqu� con su tabla de s�mbolos propia
	private Stack bloques;
	
	// Un nuevo bloque que aparezca ser� situado en el top actual de la pila y tendr� asociada su tabla de s�mbolos propia
	
	
	public SymbolsTable() {
		HashMap tablaprincipal = new HashMap();
		bloques = new Stack();
		bloques.push(tablaprincipal);
		t = (HashMap)bloques.peek();
	 }
	 
	 public VariableOrFunction insertar(VariableOrFunction s) {
		 System.out.println("   >>> PILA <<<   Insertando en pila: " + s.getIdentifier());
		 
		 t.put(s.getIdentifier(), s);
		 return s;
	 }
	 
	 public VariableOrFunction buscar(String nombre) {
		 return (VariableOrFunction)(t.get(nombre));
	 }
	 
	 public VariableOrFunction buscarGlobal(String nombre) {
		 HashMap tabla;
		 VariableOrFunction s;
		 for (int i=(bloques.size() - 1); i >= 0; i--) {
			 tabla = (HashMap) bloques.elementAt(i);
			 //System.out.println("Buscando "+nombre+" en tabla "+(i+1));
			 s = (VariableOrFunction) tabla.get(nombre);
			 if (s != null) {
				 //System.out.println("Encontrado "+nombre+" en tabla "+(i+1));
				 return s;
			 }
		 }
		 return null;
	 }
	 
	 public void set() {
		 HashMap nuevaTabla = new HashMap();
		 bloques.push(nuevaTabla);
		 t = (HashMap) bloques.peek();
	 }
	 
	 public void reset() {
		 bloques.pop();
		 t = (HashMap)bloques.peek();
	 }
	 
	 public void imprimir() {
		 System.out.println("   [ TABLA de S�MBOLOS ]   ");
		 for (int i = (bloques.size() - 1); i >= 0; i--) {
			 for (int indent = 1; indent <= i; indent++) {
				 System.out.print("\t");
			 }
			 System.out.println("   >>> BLOQUE " + (i + 1) + ":");
			 HashMap tabla = (HashMap) bloques.elementAt(i);
			 Iterator it = tabla.values().iterator();
			 while(it.hasNext()) {
				 VariableOrFunction s = (VariableOrFunction) it.next();
				 for (int indent = 0; indent <= i; indent++) {
					 System.out.print("\t");
				 }
				 System.out.println(s.toString());
			 }
		 }
	 }
	 
}
