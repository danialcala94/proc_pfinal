package es.unileon.procesadores.objects;

import java.util.ArrayList;

public class VariableOrFunction {
	private String identifier;
	private String type;
	private boolean isFunction;
	private String value;
	private int dimension;
	private ArrayList<Variable> args;
	
	public VariableOrFunction(String identifier, String tipe, boolean isFunction, String value, int dimension, ArrayList<Variable> args) {
		this.identifier = identifier;
		this.type = tipe;
		this.isFunction = isFunction;
		this.value = value;
		this.dimension = dimension;
		this.args = args;
	}
	
	public VariableOrFunction() {
		
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getType() {
		return type;
	}

	public void setType(String tipe) {
		this.type = tipe;
	}

	public boolean isFunction() {
		return isFunction;
	}

	public void setFunction(boolean isFunction) {
		this.isFunction = isFunction;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public ArrayList<Variable> getArgs() {
		return args;
	}

	public void setArgs(ArrayList<Variable> args) {
		this.args = args;
	}
	
	public String toString() {
		String str = "";
		
		if (isFunction) {
			str =  type + " _FUNCTION_ " + identifier + "[" + dimension + "] (";
			for (int i = 0; i < args.size(); i++) {
				if (i != args.size() - 1) {
					str += args.get(i).getTipo() + " " + args.get(i).getNombre() + ", ";
				} else {
					str += args.get(i).getTipo() + " " + args.get(i).getNombre();
				}
			}
			for (Variable arg : args) {
				
			}
			str += ") : " + value;
		} else {
			if (dimension != 0) {
				str = type + " " + identifier + "[" + dimension + "] : " + value;
			} else {
				str = type + " " + identifier + " " + value;
			}
		}
		
		return str;
	}

}
