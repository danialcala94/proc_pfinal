package es.unileon.procesadores.objects;

import java.util.ArrayList;

public class Function {
	String tipo = "";
	String nombre = "";
	ArrayList<Variable> argumentos = new ArrayList<Variable>();
	
	public Function(String tipo, String nombre) {
		this.tipo = tipo;
		this.nombre = nombre;
	}
	
	public Function(String tipo, String nombre, ArrayList<Variable> argumentos) {
		this.tipo = tipo;
		this.nombre = nombre;
		this.argumentos = argumentos;
	}
	
	public String getTipo() {
		return this.tipo;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public ArrayList<Variable> getValor() {
		return this.argumentos;
	}
	
	public String toString() {
		String str = "";
		
		str += this.tipo + " " + this.nombre + "(";
		for (int i = 0; i < argumentos.size(); i++) {
			str += argumentos.get(i).toString();
			if (i != (argumentos.size() - 1)) {
				str += ", ";
			}
		}
		str += ")";
		
		return str;
	}
}
