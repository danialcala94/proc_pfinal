package es.unileon.procesadores.objects;

public class Variable {
	String tipo = "";
	String nombre = "";
	String valor = "";
	
	public Variable(String tipo, String nombre) {
		this.tipo = tipo;
		this.nombre = nombre;
	}
	
	public Variable(String tipo, String nombre, String valor) {
		this.tipo = tipo;
		this.nombre = nombre;
		this.valor = valor;
	}
	
	public String getTipo() {
		return this.tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getValor() {
		return this.valor;
	}
	
	public String toString() {
		String str = "";
		
		str = this.tipo + " " + this.nombre + " " + this.valor;
		
		return str;
	}
}
