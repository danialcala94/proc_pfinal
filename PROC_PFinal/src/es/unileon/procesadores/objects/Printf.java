package es.unileon.procesadores.objects;

import java.util.ArrayList;

public class Printf {
	String texto = "";
	ArrayList<Variable> variables = new ArrayList<Variable>();
	
	public Printf(String texto) {
		this.texto = texto;
	}
	
	public Printf(String texto, ArrayList<Variable> variables) {
		this.texto = texto;
		this.variables = variables;
	}
	
	public String getTexto() {
		return this.texto;
	}
	
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public ArrayList<Variable> getVariables() {
		return this.variables;
	}
	
	public void setVariables(ArrayList<Variable> variables) {
		this.variables = variables;
	}
}
