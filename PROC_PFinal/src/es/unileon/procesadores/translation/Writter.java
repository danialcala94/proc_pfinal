package es.unileon.procesadores.translation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Writter {
	
	public static final String TRANSLATED_TO_JAVA = "files/translation.java";

	public static boolean write(String data, String filepath) {
		return xWrite(data, filepath, false);
    }
	
	public static boolean append(String data, String filepath) {
		return xWrite(data, filepath, true);
	}
	
	public static boolean translate(String data) {
		return xWrite(data, TRANSLATED_TO_JAVA, true);
	}
	
	private static boolean xWrite(String data, String filepath, boolean append) {
		File file = new File(filepath);
        FileWriter fr = null;
        try {
        	if (append) {
        		fr = new FileWriter(file, true);
        	} else {
        		fr = new FileWriter(file);
        	}
        	fr.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        return true;
	}
	
}
