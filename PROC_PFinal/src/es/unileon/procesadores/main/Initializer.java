package es.unileon.procesadores.main;

/*
 * Autores: ALCAL� VALERA, DANIEL; CRESPO TORBADO, BEATRIZ; HERRERAS G�MEZ, FRANCISCO
 * Pr�ctica: GESTI�N DE TIPOS
 */

import java.io.File;

import es.unileon.procesadores.makers.CupMaker;
import es.unileon.procesadores.makers.JFlexMaker;
import es.unileon.procesadores.parsers.Parser;

public class Initializer {

	public static void main(String[] args) {
		System.out.println("Deleting previous parsers");
		deletePreviousParsers();
	}
	
	private static void continueWorking() {
		System.out.println("Generating CUP parser file...");
		CupMaker.generateCup();
		
		System.out.println("Generating JFlex parser...");
		JFlexMaker.generateParser("files/Alexico.jflex", "src/es/unileon/procesadores/parsers/");
		
		System.out.println("Analyzing input file...");
		executeParser("files/entrada.txt");
	}
	
	private static void executeParser(String path) {
		String args[] = {path};
		Parser.main(args);
	}
	
	
	/**
	 * Deletes the previous analyzers if existing.
	 * @return
	 */
	private static void deletePreviousParsers() {
		int cont = 0;
		
		String OUTPUT_DIR = "src/es/unileon/procesadores/parsers/";
		
		if (new File(OUTPUT_DIR + "Yylex.java").delete()) {
			cont++;
		}
		if (new File(OUTPUT_DIR + "sym.java").delete()) {
			cont++;
		}
		if (new File(OUTPUT_DIR + "Parser.java").delete()) {
			cont++;
		}

		System.out.println("Deleted " + cont + " analyzers.");
		
		continueWorking();
	}

}
